{-# LANGUAGE RankNTypes #-}
module TwinGeneratorST where

import Data.Array.Unboxed
import Data.Array.ST
import Control.Monad.ST
import Control.Monad

-- A good approximate max bound for our nth prime
pmax :: Integer -> Double
pmax n' = let n = fromIntegral n' in ((n * log n) + (n * (log ((log n) - 0.9385))))

-- Applying the max bound to get a maximum Twin Prime index
pmaxt :: Integer -> Integer
pmaxt n = ceiling ((pmax n) / 6)

-- | Because this uses an array we need to give it a max bound. The Int value tells us the maximum number of primes that we want.
--   Unfortunately this use pmaxt to get an index and end up sieving out an unnecessary amount of primes.
sieve :: Int -> [Integer]
sieve x = take x (2 : 3 : (runST $ newArray (0,arrmax) True >>= \l -> newArray (0,arrmax) True >>= \r -> stSieve 0 (l,r)))
    where
        twinJump :: Integer -> Integer
        twinJump p = let n = if p `mod` 3 == 1 then 4 else 2
                     in  (((n * p) + 2) `div` 6)
        arrmax = pmaxt (fromIntegral x)
        stSieve :: Integer -> (STUArray s Integer Bool,STUArray s Integer Bool) -> ST s [Integer]
        stSieve n (larr,rarr) = do
            l <- readArray larr n
            r <- readArray rarr n
            let a = (5 + (6 * n))
                b = (7 + (6 * n))
                aix = ((a^2) - 5) `div` 6
                bix = ((b^2) - 7) `div` 6
                aixm = aix + (twinJump a)
                bixm = bix + (twinJump b)
                aixs = [aix, aix + a..arrmax]
                bixs = [bix, bix + b..arrmax]
                aixms = [aixm, aixm + a..arrmax]
                bixms = [bixm, bixm + b..arrmax]
                next = if (l && r) then (\ns -> a : b : ns) else if l then (a :) else if r then (b :) else id
            when l $ do
                mapM_ (\ix -> writeArray rarr ix False) aixs
                mapM_ (\ix -> writeArray larr ix False) aixms
            when r $ do
                mapM_ (\ix -> writeArray rarr ix False) bixs
                mapM_ (\ix -> writeArray larr ix False) bixms
            if n == arrmax
                then return []
                else fmap next $ stSieve (n+1) (larr,rarr)

-- | Get a fixed number of primes
primes :: Int -> [Integer]
primes n = sieve n
