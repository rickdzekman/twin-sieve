module Main where

import System.Environment
import qualified TwinGenerator as T
import qualified Data.Numbers.Primes as P
import qualified TwinGeneratorST as S

main :: IO ()
main = do
    (t:c:_) <- getArgs
    let n = read c :: Int
    case t of
        "primes" -> print $ (P.primes !! (n-1) :: Integer) -- Get the nth prime using the primes module
        "twin" -> print $ (T.primes !! (n-1) :: Integer) -- Get the nth prime using the purely function Twin Sieve
        "twinst" -> print $ (S.primes n !! (n-1) :: Integer) -- Get the nth prime using the ST version of the Twin Sieve