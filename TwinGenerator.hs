module TwinGenerator where

import Data.Maybe
import Data.Dequeue

-- | Generate an infinite list of Twin Prime candidates
tpCandidatePairsMaybe :: [(Maybe Integer,Maybe Integer)]
tpCandidatePairsMaybe = tpcs 11 13
    where
        tpcs x y = (Just x,Just y) : tpcs (x+6) (y+6)

-- | Calculate the position of the left hand prime elimination for some p-length sequence of Twin Prime candidates
twinJump :: Integer -> Integer
twinJump p = let n = if p `mod` 3 == 1 then 4 else 2
             in  ((((n * p) + 2) `div` 6) - 1)

-- | Check the tombstones, return primes for Just values
nextCandidate :: [(Maybe Integer,Maybe Integer)] -> BankersDequeue Integer -> [Integer]
nextCandidate ((Nothing,Nothing):cs) q = nextCandidate cs q
nextCandidate ((Just l,Nothing):cs) q = l : nextCandidate cs (pushBack q l)
nextCandidate ((Nothing,Just r):cs) q = queueChecker r q cs
nextCandidate ((Just l,Just r):cs) q = l : queueChecker r (pushBack q l) cs

-- | If we reach a right-hand Twin Prime candidate that is not a tombstone, check to see if it is
--   it is the square of the next prime in the queue
queueChecker :: Integer -> BankersDequeue Integer -> [(Maybe Integer,Maybe Integer)] -> [Integer]
queueChecker n qss cs
    | n < q^2 = n : nextCandidate cs (pushBack qss n)
    | otherwise = nextCandidate (twinIntervals (twinJump q) q cs) qs
    where
        (q',qs) = popFront qss
        q = fromJust q' -- If the queue is empty, something is very wrong

-- | Using a known prime, sieve out the appropriate left-hand and right-hand Twin Prime candidates
twinIntervals :: (Show a,Num a,Eq a) => Integer -> Integer -> [(Maybe a,Maybe a)] -> [(Maybe a,Maybe a)]
twinIntervals a b ps = twinIntervals' a (b-1) ps -- error (show (a,b,take 10 ps)) --  
    where
        twinIntervals' aa 0 ((l,_):xs) = (l,Nothing) : twinIntervals' (aa-1) (b-1) xs
        twinIntervals' 0 bb ((_,r):xs) = (Nothing,r) : twinIntervals' (b-1) (bb-1) xs
        twinIntervals' aa bb (x:xs) = x : twinIntervals' (aa-1) (bb-1) xs

-- | Generate an infintie list of primes
primes :: [Integer]
primes = 2 : 3 : 5 : 7 : nextCandidate tpCandidatePairsMaybe (fromList [5,7])
